---
layout: page
title: Charlie's Resume
permalink: /about/
---


# 简介       
李博文 (Force.Charlie)  
E-mail: forcemz@outlook.com  


# 教育  
2010.09 ~2014.06 本科 湘南学院 计算机系 通信工程  
2012.05~2014.06 年主导开发基于 ASP.NET 的 ACM 在线测评系统。

# 工作经验     
2014.06~ 至今 深圳市奥思网络科技有限公司（开源中国），软件工程师         

- 实现 GIT@OSC 代码托管平台 SVN 协议接入，属于国内第一家,基于 Java。        

- 从事 GIT 基础服务的开发，如网页爬虫（基于 Python）， GIT HTTP 智能服务，即 GIT 的 HTTP(S) 协议访问，基于 Perl，Ruby。    

- 作为核心开发者实现 GIT@OSC 分布式改造，基于 C++ 实现多线程安全的路由选择库（连接池支持）；基于 NGINX 模块开发支持 HTTP 动态代理；
基于 Boost.Asio 实现 SVN 协议动态代理服务器，   此服务器提供跨平台开源版本，即下面的 svnsrv；基于 Boost.Asio Hook 实现 git-upload-pack git-receive-pack 和
Git Smart 服务 git-smart-service ，取代 git 默认的 upload-pack/receive-pack 从而使得 Git 无论是 HTTP smart 还是 SSH 还是 Git 协议都能通过动态代理实现分布式。

# 技能     
包括但不限于以下语言和框架：  
语言:        

- C/C++     

- C#   

- PowerShell     

- D     

- Perl

- Shell     

- Python   

- Makefile   

- Ruby       

- Java      

框架:     

- Boost Boost.Asio   

- Qt  

- WTL   

- Win32  

- ASP.NET   

- WPF      

- webpy   

- nginx Perl   

- Vibe.d   

- Rails  

- Puma  

 操作系统：   

- Linux ,
多线程，多进程，网络编程。   

- Windows 
Win32 程序开发，主要是 GUI 程序。  

- FreeBSD   

- ReactOS   

- Minix  

- Hurd  

- Haiku  


通常来说，本人精通 git 和 svn。  


# 个人作品    
博客站点 [http://forcemz.net/](http://forcemz.net/)         

- Clangbuilder 基于 PowerShell 开发的 Windows 平台 LLVM 自动构建工具集。  
   [https://github.com/fstudio/clangbuilder](https://github.com/fstudio/clangbuilder)      

- svnsrv 开源跨平台的 svn 协议动态代理服务器。  
   [http://git.oschina.net/oschina/svnsrv](http://git.oschina.net/oschina/svnsrv)     
   
- iBurnMgr 基于 C++ & Direct2D 开发的 USB 启动盘制作软件。  
   [https://github.com/fcharlie/iBurnMgr](https://github.com/fcharlie/iBurnMgr)      
   
- Ginkgo 基于 WPF 开发的 Metro 风格 Batch 编辑器。  
   [https://github.com/fstudio/Ginkgo](https://github.com/fstudio/Ginkgo)      
   
- whois Windows 平台 whois 实现。   
   [http://git.oschina.net/ipvb/whois](http://git.oschina.net/ipvb/whois)    
   
- PE Analyzer 基于 C++ & Direct2D 开发的 PE 格式检查软件。  
   [https://github.com/fcharlie/PEAnalyzer](https://github.com/fcharlie/PEAnalyzer)        
    
- MsysLauncher MSYS2 运行环境启动器。  
   [https://github.com/fcharlie/msys2-launcher](https://github.com/fcharlie/msys2-launcher)     
   
- WiFiAssistant Windows 平台 WiFi 无线承载网络开启助手。  
   [https://github.com/fcharlie/WiFiAssistant](https://github.com/fcharlie/WiFiAssistant)     
   
- Exile 基于 Microsoft cpprestsdk 以及 HTTP.sys 的 Git Smart HTTP Server。   
   [https://github.com/fstudio/Exile](https://github.com/fstudio/Exile)     

# 其他
- bmake BSD make 移植到 Windows 平台，欢迎参与 ！ [https://github.com/fstudio/bmake](https://github.com/fstudio/bmake)   
